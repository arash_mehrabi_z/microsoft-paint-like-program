import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.Math ;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;


public class DrawShape extends JPanel implements MouseInputListener, ActionListener,ItemListener, Serializable  {

	
	private int x ; //x coordinate of when mouse pressed
	private int y ; // ...
	private int x2 ; // x coordinate of whend mouse released
	private int y2 ; // ...
	private int height ;
	private int width ;
	ArrayList<DrawShape> arrli = new ArrayList<DrawShape>(100);
	private static int i1=0 ;
	private int type ; // type 1 == rectangle , type 2 == circle   , type 3 == line
	private boolean fill ;
	private static int j ;
	
	@Override
	public void paintComponent(Graphics g) //method for drawing shapes
	{
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		
		for(int i=0 ; i<arrli.size(); i++)
		{	
			if(arrli.get(i).type==1)
			{
				if(arrli.get(i).fill == false)
					g.drawRect(arrli.get(i).x, arrli.get(i).y, arrli.get(i).width, arrli.get(i).height);
				else
					g.fillRect(arrli.get(i).x, arrli.get(i).y, arrli.get(i).width, arrli.get(i).height);
			}
			else if (arrli.get(i).type==3) // for line
				g.drawLine(arrli.get(i).x, arrli.get(i).y, arrli.get(i).x2, arrli.get(i).y2);
			
			else if(arrli.get(i).type==2) // for circle
				if(arrli.get(i).fill == false)
					g.drawOval(arrli.get(i).x, arrli.get(i).y, arrli.get(i).width, arrli.get(i).width);
				else
					g.fillOval(arrli.get(i).x, arrli.get(i).y, arrli.get(i).width, arrli.get(i).width);
		
			
		}
		
	}
	
	public DrawShape(int x , int y , int x2 , int y2 , int height , int width , int type , boolean fill) //constructor
	{
		this.x = x ;
		this.y = y ;
		this.y2 = y2 ;
		this.x2 = x2 ;
		this.height = height ;
		this.width = width ;
		this.type = type ;
		this.fill = fill ;
		
		//adding Mouse Listener
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) { //method for mouse handling
		
		this.x = e.getX();
		this.y = e.getY();
		
	}

	@Override
	public void mouseReleased(MouseEvent e) { //method for mouse handling
		this.x2 = e.getX() ;
		this.y2 = e.getY() ;
		this.height = Math.abs(y2-y) ;
		this.width = Math.abs(x2-x) ;
		DrawShape shape = new DrawShape ( x , y , x2 , y2 , height , width , type , fill) ; //creating objects for saving them
		arrli.add(i1++,shape); // "i" is static int 
	//	System.out.println(i) ;
		//revalidate() ;
		repaint() ;
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand()=="Text")
		{
			TextFrame frame = new TextFrame() ;
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(500,500);
			frame.setVisible(true);
		}
		if(e.getActionCommand()=="Rectangle")
			 this.type = 1;
		
		if(e.getActionCommand()==" Circle       ")
			this.type = 2;
		
		if(e.getActionCommand()==" Line          ")
			this.type = 3;
		
		// for saving & loading
		
		if(e.getActionCommand()==" Save         ")
			{
			FileOutputStream fos = null ;
			try {
				fos = new FileOutputStream("t.tmp");
			}
				
			 catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ObjectOutputStream oos = null;
				try {
					oos = new ObjectOutputStream(fos);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					oos.writeObject(arrli);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					oos.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		// for loading
		if(e.getActionCommand()==" Load         ")
		{
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("t.tmp");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ObjectInputStream ois = null;
			try {
				ois = new ObjectInputStream(fis);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				arrli = (ArrayList<DrawShape>) ois.readObject();
			} catch (ClassNotFoundException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				ois.close();
				repaint() ;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		
		++j ;
		if(j%2==1)
		{
			this.fill=true ;
		}
		else
			this.fill = false ;
	}

}
