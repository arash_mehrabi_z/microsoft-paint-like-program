import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

public class Main{


	public static void main(String[] args) {

		JPanel panel =  new DrawShape(0,0,0,0, 0 , 0, 0 , false) ;
		JPanel subpanel = new JPanel(); // subpanel for buttons
		panel.setBackground(Color.WHITE);
		
		JButton rectangle = new JButton("Rectangle") ;
		JButton line = new JButton(" Line          ") ;
		JButton circle = new JButton(" Circle       ") ;
		JButton save = new JButton(" Save         ") ;
		JButton load = new JButton(" Load         ") ;
		JButton text = new JButton("Text") ;
		JCheckBox fill =  new JCheckBox("Fill"); 
		
		rectangle.addActionListener((ActionListener) panel); //action listener for button
		line.addActionListener((ActionListener) panel);
		circle.addActionListener((ActionListener) panel);
		save.addActionListener((ActionListener) panel);
		load.addActionListener((ActionListener) panel);
		text.addActionListener((ActionListener) panel);
		fill.addItemListener((ItemListener) panel);

		subpanel.setLayout(new BoxLayout(subpanel , BoxLayout.Y_AXIS));
		subpanel.add(rectangle) ;
		subpanel.add(line) ;
		subpanel.add(circle) ;
		subpanel.add(save) ;
		subpanel.add(load) ;
		subpanel.add(fill) ;
		subpanel.add(text) ;
		
		JFrame frame = new JFrame() ;
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		
		frame.add(panel) ;
		frame.add(subpanel , BorderLayout.WEST) ;
		frame.setVisible(true);

	}	

}