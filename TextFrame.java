import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class TextFrame extends JFrame {
	
	private JTextField text ;
	private JCheckBox bold ;
	private JCheckBox italic ;
	private JComboBox	 nameCombo;
	private JList		 colorList;
	private JScrollPane	 myScrool;
	
	private int fontState;
	private String fontName="Arial";
	private Color fontColor=Color.BLACK;
	Color[]  colors= {new Color(255,0,0),Color.GREEN,Color.BLUE,Color.WHITE,Color.BLACK , Color.YELLOW };
	String[] colorNames={"Red","Green","Blue","White","Black","Yellow"};
	
	public TextFrame()
	{
		super("Text");
		setLayout(new FlowLayout()) ;
		
		text = new JTextField("      Sample      ") ;
		bold = new JCheckBox("Bold") ;
		italic = new JCheckBox("Italic") ;
		//get all fonts
		String[] fontNames=
				GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		nameCombo=new JComboBox<String>(fontNames);
		nameCombo.setMaximumRowCount(4);
	
		
		Handler handler = new Handler() ;
		bold.addItemListener(handler);
		italic.addItemListener(handler);
		nameCombo.addItemListener(handler);
		
		//color
		colorList=new JList<String>(colorNames);
		colorList.setVisibleRowCount(4);
		colorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		myScrool=new JScrollPane(colorList);
		colorList.addListSelectionListener(handler);
		
		add(text);
		add(bold) ;
		add(italic) ;
		add(nameCombo) ;
		add(myScrool);
	}
	
	void Refresh()
	{
		text.setFont(new Font(fontName,fontState,14));
		text.setForeground(fontColor);
	}
	
	private class Handler implements ItemListener , ListSelectionListener
	{
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(italic.isSelected())
				fontState = Font.ITALIC ;
			if(bold.isSelected())
				fontState = Font.BOLD ;
			if(bold.isSelected() && italic.isSelected())
				fontState = Font.ITALIC + Font.BOLD ;
			if(!bold.isSelected() && !italic.isSelected())
				fontState=0 ;
			fontName=(String) nameCombo.getSelectedItem();
			Refresh() ;
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			fontColor= colors[ e.getFirstIndex()];
			Refresh();
			
		}
		
	}

}
